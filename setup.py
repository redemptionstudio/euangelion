import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__fie__),fname)).read()

setup(name='euangelion',
        version='3.0.0',
        description='Euganelion Python Encrypted CLI MTA',
        long_description=read('README.md'),
        url='https://www.redemption.studio',
        author='T. H. Wright',
        author_email='git@redemption.studio',
        license='GPL v3',
        packages=['euangelion'],
        zip_safe=False)
