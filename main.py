#!/usr/bin/python3

################################
# Includes/Libraries
import sys
import os
import configparser
from os.path import expanduser
import smtplib
import gnupg
#
################################

################################
# Assign Variables
def initialize(Receiver,Header,Content):
    # Creates a variable for storing the executing user's home directory
    global content, encdata, gpg, header, HOME, rkeys, save, tmp
    HOME = expanduser("~")
    # Tells Python-GnuPG where to find the user's key ring.
    gpg = gnupg.GPG(gnupghome=HOME + "/.gnupg")
    # Tells Python-GnuPG which email address's key we should use for encryption based on user's key ring.
    rkeys = Receiver
    # A file used for storing the contents of our message.
    tmp = '/tmp/euangelion.tmp'
    # A file used for storing the encrypted contents of our message.
    save = '/tmp/euangelion.asc'
    # A variable which tells the MTA who is sending what to whom.
    header = Header
    # A variable which contains the meat of our message, later to be encrypted.
    content = Content
################################

################################
# Send SMTP Auth Mail Function
def mail(Server,Port,User,Password,Sender,Receiver,Message):
    try:
        server = smtplib.SMTP(Server,Port)
        server.starttls()
        server.login(User, Password)
        server.sendmail(Sender, Receiver, Message)
#        print("Message sent.")
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise
################################

################################
# Encrypt Message Function
def encrypt(Content):
    global body
    # Writes the email contents to tmp, a file. This should eventually be replaced by the actual contents of a file on the server based on the program calling the script.
    with open(tmp, "w+") as f0:
        f0.write(Content)
    # Encrypts the contents of tmp, a file, for the user specified in rkeys and outputs that data to save, a file.
    with open(tmp, "rb") as f1:
        encdata = gpg.encrypt_file(f1, rkeys.split(), always_trust=True, output=save)
    # Reads save, a file, and assigns its contents to body.
    with open(save, "r") as f2:
        body = f2.read()
################################

################################
# Main Program
def sendCLI(Server,Port,User,Password,Sender,Receiver,Header,Content):
    ################################
    # Variable Declaration
    initialize(Receiver,Header,Content)
    ################################

    ################################
    # File Handling.
    encrypt(content)
    ################################

    ################################
    # Message creation.
    # The message variable is composed of the header values for an email as well as the encrypted contents stored in body.
    message = header + body
    # The mail function is called to send to recipient the combined value of message.
    mail(Server,Port,User,Password,Sender,Receiver,message)
    ################################

    ################################
    # Cleanup Files
    if os.path.exists(tmp):
        os.remove(tmp)
    else:
        print ("Unencrypted file does not exist.")
    if os.path.exists(save):
        os.remove(save)
    else:
        print ("Encrypted file does not exist.")
    ################################

def main():
    parser = configparser.SafeConfigParser()
    parser.read('/opt/euangelion/euangelionmail.ini')
    Server = parser.get('DEFAULT','Server')
    Port = parser.get('DEFAULT','Port')
    Username = parser.get('DEFAULT','Username')
    Password = parser.get('DEFAULT','Password')
    Sender = parser.get('DEFAULT','Sender')
    Receiver = parser.get('DEFAULT','Receiver')
    From_Name = parser.get('SCRIPT','From_Name')
    From = parser.get('SCRIPT','From')
    To_Name = parser.get('SCRIPT','To_Name')
    To = parser.get('SCRIPT','To')
    Subject = parser.get('SCRIPT','Subject')

    lines = []

    for line in sys.stdin:
        lines.append(line)

    Content = ''.join(lines)
    Header = """From: %s <%s>
To: %s <%s>
Subject: %s

""" % (From_Name, From, To_Name, To, Subject)

    sendCLI(Server, Port, Username, Password, Sender, Receiver, Header, Content)

main()
