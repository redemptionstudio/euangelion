#!/usr/bin/python3.6

################################
# Information
TITLE="Euangelion Mail Service Install"
VERSION="1.0.0"
DATE="20170613"
AUTHOR="T. H. Wright"
DESC="Sets up the config file for Euangelion"
#
# Must be run as root unless /opt/euangelion is owned by the executing user
# https://stackoverflow.com/questions/12332975/installing-python-module-within-code
# If configparser is not installed, run `pip install configparser` as root
# If libgpgme is not installed, run `apt install libgpgme-dev`
# If gpg is not installed, run `apt install python3-gnupg` and `pip install gnupg`
#
################################

config = configparser.ConfigParser()

configdir = '/opt/euangelion'
if not os.path.exists(configdir):
    os.makedirs(configdir)

config['DEFAULT'] = {'Server': 'mail.domain.tld',
        'Port': '587',
        'Username' : 'user@domain.tld',
        'Password': 'userspassword',
        'Sender': 'user@domain.tld',
        'Receiver': 'recipient@domain.tld'}
config['SCRIPT'] = {'From_Name': 'Euangelion Mail Service',
        'From': 'user@domain.tld',
        'To_Name': 'System Administrator',
        'To': 'user@domain.tld',
        'Subject': 'Euangelion Mail Service'}
with open(configdir + '/euangelionmail.ini', 'w') as configfile:
    config.write(configfile)

os.chmod(configdir, 0o700)
for root, dirs, files in os.walk(configdir):
    for d in dirs:
        os.chmod(os.path.join(root, d), 0o700)
    for f in files:
        os.chmod(os.path.join(root, f), 0o600)

