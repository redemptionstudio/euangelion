# <a href="https://www.redemption.studio/t/euangelion-encrypted-cli-mta/613">Euangelion Python Encrypted CLI SMTP Client</a>
## By T. H. Wright, Redemption Studio
 
<a href="https://www.redemption.studio/t/euangelion-encrypted-cli-mta/613">Euangelion</a> is a CLI program that receives piped input and emails through a preconfigured recipient an encrypted message via GPG.

## About
 
Euangelion was programmed utilizing Python 3.6. It was not only a learner's project developed first in June 2017 to relearn Python but to add more to the author's programming skills. It intentionally pulls from a .ini file rather than defining these in the install itself.

### How to Use

Once installed, run /path/to/euangelion/install.py.

Edit /opt/eungelion/euangelionmail.ini with your mail server.

Include the script in the CLI as the recipient of piped command output.

    someCommand | /path/to/euangelion/main.py

## Install

In order to function, a Linux user must run "gpg --import /path/to/public.key" in order to bring a user's public key into available keys that the python-gnupg library can see and find

### Ubuntu
If configparser is not installed, run `pip install configparser` as root  
If libgpgme is not installed, run `apt install libgpgme-dev`  
If gpg is not installed, run `apt install python3-gnupg` and `pip install gnupg`  

## Notes

This script has only been tested on Linux. It was originally programmed on a Fedora workstation and later moved to an Ubuntu workstation.

### Known Issues

* No error checking in place
* Setup.py/Automatic install incomplete
* Main.py main() could be cleaned up
